// create an express app
const express = require("express");
const cors = require("cors");
const axios = require("axios");
const app = express();

app.use(express());

app.use(cors());

app.use("/play/:channel", (req, res) => {
    let channel = req.params.channel;
    let play = req.query.play || false;
    axios.get('https://streamia.xyz/embed/'+channel, {
        headers: {
            Referer: 'https://sportshd.co'
        }
    }).then((response) => {
        function search(edata, searchtxt) {
            let re = new RegExp(
                "((\\S+[\\b\\s]?)" +
                searchtxt +
                "([\\b\\s]?\\S+))",
                "i"
                );
            let matches = edata.match(re);
            if (matches) {
                let words = edata.match(re)[0].split(/\s+/);
                return words.join(" ");
            }
        }
        let data = response.data;
        data =  data.replaceAll(`","`, ``).replaceAll(` + `, ``);
        let link = search(data, "streamia");
        link = link.replaceAll(`(["`, ``).replaceAll(`"]`,``).replaceAll(`("")`,``).replaceAll(`document.getElementById`, ``).replaceAll(`("`, ``).replaceAll(`").innerHTML);`, ``);
        link = link.split(`.join`);
        let p1 = link[0];
        let p2 = link[1];
        let p3  = link[2];

        data = data.replaceAll(` = `, ``);
        link = search(data, p2);
        p2 = link.replace(`var`,``).replace(p2, ``).replace(` `, ``).replace(`["`, ``).replace(`"];`, ``);

        link = search(data, p3);
        p3 = link.replace(`id=`,``).replace(p3, ``).replace(` `, ``).replace(`</span><span`, ``).replace(`"];`, ``);
        p3 = p3.slice(1);

        link = p1+p2+p3;
        link = link.replaceAll(`\\`, ``);
        if (play) {
            res.send(`<!DOCTYPE html>
<html lang="en">  
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <title>${channel.toUpperCase().replaceAll('-', '')} Live Stream</title>
            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@clappr/player@0.4.7/dist/clappr.min.js"></script>
                <style>
        body {
            margin: 0;
            padding: 0;
        }

        #player {
            height: 100vh;
        }
        .cc-controls[data-cc-controls].available {
            display: none !important; 
        }
    </style>
    
    </head>
    <body>
        <div id="player"></div>
        <script type="text/javascript" defer>
        function load() {
            var retryCount = 0;
            var player = new Clappr.Player({
                source: "${link}",
                parentId: "#player",
                width: '100%',
                height: '100%',
                muted: false,
                autoPlay: true,
                mediacontrol: {seekbar: "#00ffff"},
                disableVideoTagContextMenu: true,
                events: {
                    onError: function(e) {
                        if(retryCount < 3) {
                            retryCount++;
                            setTimeout(function() { player.configure(player.options); }, 1000);
                            return;
                        }
                    }
                },
                watermark: "https://amsappdb.000webhostapp.com/logo.png", position: 'top-left',
                                })
        }
            load();
        </script>
    </body>
</html>
            `)
        }
        else {
            res.send({link}).end();
        }   
    }).catch((error) => {
        res.send("Something went wrong");
    })
});

app.use("/", (req, res) => {
    res.send("API is running")
})


app.listen(process.env.PORT || 5000, () => console.log("Server is running..."));